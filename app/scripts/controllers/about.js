'use strict';

/**
 * @ngdoc function
 * @name agentDashApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the agentDashApp
 */
angular.module('agentDashApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
