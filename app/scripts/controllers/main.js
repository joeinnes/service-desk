'use strict';

/**
 * @ngdoc function
 * @name agentDashApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the agentDashApp
 */
angular.module('agentDashApp')
  .controller('MainCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  })

  .controller('CurrentTickets', function ($scope) {
    // Object lists current status of agent's tickets
    $scope.tickets = {
      assigned: 1,
      wip: 2,
      pending: 3
    };
  })

  .controller('TicketStats', function ($scope) {
    // Object lists stats for agent's tickets
    $scope.tickets = {
      hour: 5,
      day: 25,
      week: 400
    };
  })

  .controller('AlertTicker', function ($scope) {
    // Object lists alerts
    $scope.alerts = [{
        type: 'critical',
<<<<<<< HEAD
        location: 'Emerald City',
=======
        location: 'Leven',
>>>>>>> 6116c53dd59b4b358209fc77a9af3557e3aab5aa
        text: 'Network down',
        inc: 'INC8675309',
        raisedBy: 'Joe Innes'
      },
      {
        type: 'normal',
        text: 'Do your timesheets!',
<<<<<<< HEAD
        raisedBy: 'Project lead'
      },
      {
        type: 'critical',
        location: 'Gotham City',
        text: 'Can\'t print from Wifi',
        inc: 'INC123456',
        raisedBy: 'Agent X'
=======
        raisedBy: 'Jose Espinosa'
      },
      {
        type: 'critical',
        location: 'Black River Park',
        text: 'Can\'t print from SAP - DEP',
        inc: 'INC123456',
        raisedBy: 'Joe McCann'
>>>>>>> 6116c53dd59b4b358209fc77a9af3557e3aab5aa
      },
      {
        type: 'normal',
        text: 'Christmas party on 5th December'
<<<<<<< HEAD
      },
      {
        type: 'critical',
        location: 'Metropolis',
        text: 'Kryptonite in the server room',
        inc: 'INC123456',
        raisedBy: 'Lex Luthor'
      },];
=======
      }];
>>>>>>> 6116c53dd59b4b358209fc77a9af3557e3aab5aa
  })

  .controller('UpdateTicker', function ($scope) {
    // Object lists chat updates
    $scope.updates = [{
      text: 'Hey, anyone know how to get rid of the Outlook disconnected error message?',
      author: 'Joe Innes',
      image: 'http://placehold.it/32x32'
    }, {
      text: 'Process update - lost/stolen/broken mobile in Spain',
<<<<<<< HEAD
      author: 'Knowledge Manager',
      image: 'http://placehold.it/32x32'
    }, {
      text: 'Who\'s up for the pub tonight?',
      author: 'Agent Z',
      image: 'http://placehold.it/32x32'
    }, {
      text: 'Can\'t log on to VPN from home - knowledge article updated',
      author: 'Knowledge Manager',
      image: 'http://placehold.it/32x32'
    }, {
      text: 'Check your tickets, guys!',
      author: 'Queue Manager',
      image: 'http://placehold.it/32x32'
    }, {
      text: 'Morning all',
      author: 'Friendly agent',
=======
      author: 'Eva Palotas',
      image: 'http://placehold.it/32x32'
    }, {
      text: 'Who\'s up for Kamra tonight?',
      author: 'David Nelson',
      image: 'http://placehold.it/32x32'
    }, {
      text: 'Can\'t log on to RAS from home',
      author: 'Marty McFly',
      image: 'http://placehold.it/32x32'
    }, {
      text: 'Check your tickets, guys!',
      author: 'Yasser',
      image: 'http://placehold.it/32x32'
    }, {
      text: 'Kurva anyád',
      author: 'Attila Homorodi',
>>>>>>> 6116c53dd59b4b358209fc77a9af3557e3aab5aa
      image: 'http://placehold.it/32x32'
    }];
  })

  .controller('ToDo', function ($scope) {
    // Object lists todos
    $scope.todos = [{
      task: 'Contact John about INC012345',
      done: false
    }, {
      task: 'Contact Jenny about INC012346',
      done: false
    }, {
      task: 'Contact Dave about INC012347',
      done: true
    }, {
      task: 'Contact Sam about INC012348',
      done: false
    }, {
      task: 'Contact Emily about INC012349',
      done: false
    }, {
      task: 'Contact Emily about INC012349',
      done: false
    }, {
      task: 'Contact Emily about INC012349',
      done: false
    }, {
      task: 'Contact Emily about INC012349',
      done: false
    }, {
      task: 'Contact Emily about INC012349',
      done: false
    }, {
      task: 'Contact Emily about INC012349',
      done: false
    }, {
      task: 'Contact Emily about INC012349',
      done: false
    }, {
      task: 'Contact Emily about INC012349',
      done: false
    }, {
      task: 'Contact Emily about INC012349',
      done: false
    }, {
      task: 'Contact Emily about INC012349',
      done: false
    }, {
      task: 'Contact Emily about INC012349',
      done: false
    }, {
      task: 'Contact Emily about INC012349',
      done: false
    }, {
      task: 'Contact Emily about INC012349',
      done: false
    }, {
      task: 'Contact Emily about INC012349',
      done: false
    }, {
      task: 'Contact Emily about INC012349',
      done: false
    }, {
      task: 'Contact Emily about INC012349',
      done: false
    }, {
      task: 'Contact Emily about INC012349',
      done: false
    }, {
      task: 'Contact Emily about INC012349',
      done: false
    }, {
      task: 'Contact Emily about INC012349',
      done: false
    }];
  });
